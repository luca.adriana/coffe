package com.sda.adriana.project.thymeleaf.thymeleafdemo.service;

import com.sda.adriana.project.thymeleaf.thymeleafdemo.entity.User;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;

@Service
public interface UserService {

    User createUser(User user);

    List<User> getAllUsers();

    Optional<User> getUserById(Long id);

    User updateUser(User user);

    void deleteUser(Long user);
}
