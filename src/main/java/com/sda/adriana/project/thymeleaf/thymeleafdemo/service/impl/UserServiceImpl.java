package com.sda.adriana.project.thymeleaf.thymeleafdemo.service.impl;

import com.sda.adriana.project.thymeleaf.thymeleafdemo.entity.User;
import com.sda.adriana.project.thymeleaf.thymeleafdemo.repository.UserRepository;
import com.sda.adriana.project.thymeleaf.thymeleafdemo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

//@Service
@Component
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    //  @Override
    public User createUser(User user) {
        return userRepository.saveAndFlush(user);
    }

    //  @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    //  @Override
    public Optional<User> getUserById(Long id) {
        return userRepository.findById(id);
    }

    // @Override
    public User updateUser(User user) {
        return userRepository.saveAndFlush(user);
    }

    // @Override
    public void deleteUser(Long id) {
        userRepository.deleteById(id);

    }
}
