package com.sda.adriana.project.thymeleaf.thymeleafdemo.entity;

import lombok.Data;

import javax.persistence.Entity;

@Entity
@Data
public class Item {

    private Long id;
    private String name;
}
