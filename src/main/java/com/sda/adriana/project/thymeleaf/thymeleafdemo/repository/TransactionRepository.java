package com.sda.adriana.project.thymeleaf.thymeleafdemo.repository;

import com.sda.adriana.project.thymeleaf.thymeleafdemo.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {
}
