package com.sda.adriana.project.thymeleaf.thymeleafdemo.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data //le contine pe toate
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String firstName;
    @Column
    private String lastName;
    @Column
    private String username;
    @Column
    private String password;
    @Column
    private String email;
    @Column
    private String adress;
    @Column
    private Integer age;
    @Column
    private Integer cnp;
    @Column
    private LocalDate startDate;


}
