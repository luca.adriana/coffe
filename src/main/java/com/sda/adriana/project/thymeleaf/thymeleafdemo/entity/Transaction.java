package com.sda.adriana.project.thymeleaf.thymeleafdemo.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Data
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private LocalDateTime timeAndDate;
    @Column
    private Integer cardNumber;
    @Column
    private Short cvv;
    @Column
    private String deliveryAdress;
    @Column
    private Integer totalPrice;
    @Column
    private LocalDate dateReceive;


}
